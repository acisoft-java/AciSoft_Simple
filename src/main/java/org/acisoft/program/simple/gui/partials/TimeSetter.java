package org.acisoft.program.simple.gui.partials;

import org.acisoft.core.AciPlayer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Spinner;
import org.acisoft.core.PlayState;
import org.acisoft.core.Time;
import org.acisoft.exception.IllegalStateException;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class TimeSetter extends Composite {
	private Spinner spnHours;
	private Spinner spnMinutes;
	private Spinner spnSeconds;
	private Spinner spnMilliseconds;
	private Button btnApply;

	private AciPlayer aciPlayer;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public TimeSetter(Composite parent, int style) {
		this(parent, style, null);
	}

	/**
	 * Create the composite.
	 *  @param parent
	 * @param style
	 * @param aciPlayer
	 */
	public TimeSetter(Composite parent, int style, AciPlayer aciPlayer) {
		super(parent, SWT.NONE);

		this.aciPlayer = aciPlayer;

		setLayout(new GridLayout(8, false));

		spnHours = new Spinner(this, SWT.BORDER);
		GridData gd_spnHours = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spnHours.widthHint = 25;
		spnHours.setLayoutData(gd_spnHours);

		Label label = new Label(this, SWT.NONE);
		label.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		label.setText(":");

		spnMinutes = new Spinner(this, SWT.BORDER);
		GridData gd_spnMinutes = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spnMinutes.widthHint = 15;
		spnMinutes.setLayoutData(gd_spnMinutes);
		spnMinutes.setMaximum(59);

		Label label_1 = new Label(this, SWT.NONE);
		label_1.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		label_1.setText(":");

		spnSeconds = new Spinner(this, SWT.BORDER);
		GridData gd_spnSeconds = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spnSeconds.widthHint = 15;
		spnSeconds.setLayoutData(gd_spnSeconds);
		spnSeconds.setMaximum(59);

		Label label_2 = new Label(this, SWT.NONE);
		label_2.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		label_2.setText(",");

		spnMilliseconds = new Spinner(this, SWT.BORDER);
		GridData gd_spnMilliseconds = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spnMilliseconds.widthHint = 25;
		spnMilliseconds.setLayoutData(gd_spnMilliseconds);
		spnMilliseconds.setMaximum(999);

		btnApply = new Button(this, SWT.NONE);
		btnApply.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				applyClick(e);
			}
		});
		btnApply.setEnabled(false);
		btnApply.setText(Lang._txt()._TimeSetter_SetButton());

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	public boolean isEnabled() {
		return this.getEnabled();
	}

	@Override
	public boolean getEnabled() {
		return this.btnApply.getEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.btnApply.setEnabled(enabled);
	}

	public void applyClick(MouseEvent e) {
		PlayState state = PlayState.UNKNOWN;
		try {
			state = this.aciPlayer.getPlayState();
		} catch (IllegalStateException err) {
			err.printStackTrace();
			return;
		}
		if (state != PlayState.PLAYING && state != PlayState.PAUSED) return;

		Time lengthObj = this.aciPlayer.getLoadedMedia().getLength();
		int length = lengthObj.toMilliseconds();

		long hours = this.spnHours.getSelection();
		int minutes = this.spnMinutes.getSelection();
		int seconds = this.spnSeconds.getSelection();
		int milliseconds = this.spnMilliseconds.getSelection();
		Time time = new Time(hours, minutes, seconds, milliseconds);

		if (time.toMilliseconds() > length) time = Time.fromMilliseconds(length);
		this.aciPlayer.setTime(time);
	}
}

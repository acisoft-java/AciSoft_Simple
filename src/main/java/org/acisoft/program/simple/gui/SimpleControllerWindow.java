package org.acisoft.program.simple.gui;

import org.acisoft.core.AciPlayer;
import org.acisoft.core.MediaEntryFactory;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.acisoft.core.Scheduler;
import org.acisoft.core.Updateable;
import org.acisoft.core.gui.Window;
import org.acisoft.program.simple.AciSoftSimple;
import org.acisoft.program.simple.gui.partials.MediaFileCollection;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellEvent;
import org.acisoft.program.simple.gui.partials.MonitorManage;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.acisoft.program.simple.gui.partials.PlayerControls;
import org.acisoft.program.simple.gui.partials.TimeControl;
import org.acisoft.program.simple.lang.Lang;
import org.acisoft.program.simple.lang.LanguagePack;
import org.acisoft.program.simple.lang.LanguagePackContainer;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleControllerWindow extends Window implements Updateable {
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleControllerWindow.class);

	private MonitorManage monitorManage;
	private MediaFileCollection mediaFileSelectionGroup;
	private ScrolledComposite scrolledComposite;

	private PlayerControls playerControls;
	private TimeControl timeControl;
	private Menu menu;
	private MenuItem mntmLanguage;
	private Menu menuDropDownLanguage;
	private List<MenuItem> menuLanguages;
	private AciSoftSimple aciSoftSimple;
	private AciPlayer aciPlayer;
	private MediaEntryFactory mediaEntryFactory;

	/**
	 * Create the shell.
	 *
     * @param display
     * @param aciSoftSimple
     */
	public SimpleControllerWindow(Display display, AciSoftSimple aciSoftSimple, AciPlayer aciPlayer,
                                  MediaEntryFactory mediaEntryFactory) {
		super(display, SWT.SHELL_TRIM);
        this.aciSoftSimple = aciSoftSimple;
        setMinimumSize(new Point(500, 570));
		setLayout(new GridLayout(1, false));

		this.aciPlayer = aciPlayer;
		this.mediaEntryFactory = mediaEntryFactory;

		Scheduler.register(this);

		this.setIcon();
		createContents();
		update();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText(AciSoftSimple.APP_NAME + " v" + AciSoftSimple.VERSION);
		setSize(500, 570);

		this.createMenu();

		monitorManage = new MonitorManage(this, SWT.NONE, aciPlayer);
		monitorManage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		playerControls = new PlayerControls(this, SWT.NONE, aciPlayer);
		playerControls.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		playerControls.setText(Lang._txt()._PlayerControls_Title());

		timeControl = new TimeControl(this, SWT.NONE, aciPlayer);
		timeControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		timeControl.setText(Lang._txt()._TimeControls_Title());

		scrolledComposite = new ScrolledComposite(this, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		mediaFileSelectionGroup = new MediaFileCollection(scrolledComposite, SWT.NONE, this, aciPlayer, mediaEntryFactory);
		mediaFileSelectionGroup.setEnabled(false);
		scrolledComposite.setContent(mediaFileSelectionGroup);
		scrolledComposite.setMinSize(mediaFileSelectionGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	private void createMenu() {
		menu = new Menu(this, SWT.BAR);
		setMenuBar(menu);

		mntmLanguage = new MenuItem(menu, SWT.CASCADE);
		mntmLanguage.setText(Lang._txt()._Menu_Language());

		menuDropDownLanguage = new Menu(mntmLanguage);
		mntmLanguage.setMenu(menuDropDownLanguage);
		
		menuLanguages = new ArrayList<>();

		// Get available languages
		List<Class<? extends LanguagePackContainer>> availableLangs = LanguagePack.getAvailableLanguages();
		// Get currently set language
		Class<? extends LanguagePackContainer> currentLanguage = LanguagePack.getCurrentLanguage();
		// Iterate languages
		for (Class<? extends LanguagePackContainer> lang : availableLangs) {
			MenuItem menuItemLang = new MenuItem(menuDropDownLanguage, SWT.RADIO);
			menuLanguages.add(menuItemLang);
			
			// Add meta data
			menuItemLang.setData("language", lang);

			String text = this.getMenuItemTextForLanguage(lang);
			if (text == null) {
				menuItemLang.setText("???");
				menuItemLang.setEnabled(false);
			} else {
				menuItemLang.setText(text);
			}

			if (currentLanguage.equals(lang)) {
				menuItemLang.setSelection(true);
				menuItemLang.setEnabled(false);
			}

			menuItemLang.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					if (menuItemLang.getSelection()) {
						setLanguage(lang);
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
				}
			});
		}
	}

	private void setIcon() {
		/*
		 * TODO Icon final String iconPath = "/img/AciSoft_Simple.png";
		 * InputStream imgStream =
		 * this.getClass().getResourceAsStream(iconPath);
		 * 
		 * if (imgStream == null) { throw new NullPointerException(); }
		 * 
		 * Image icon = new Image(this.getDisplay(), imgStream);
		 * this.setImage(icon);
		 */
	}

	public String getMenuItemTextForLanguage(Class<? extends LanguagePackContainer> languageContainerClass) {
		LanguagePackContainer lang = LanguagePack.getInstanceOfLanguage(languageContainerClass);
		if (lang == null) {
			return null;
		}

		String langName = lang.getLanguageName();
		String langNameInEnglish = lang.getLanguageNameInEnglish();

		if (langName.equals(langNameInEnglish)) {
			return langName;
		} else {
			return langName + " (" + langNameInEnglish + ")";
		}
	}

	public void setLanguage(Class<? extends LanguagePackContainer> languageContainerClass) {
		//LanguagePack.set(languageContainerClass); // Don't: languages will mix up in the interface
		try {
			LanguagePack.saveToDisk(languageContainerClass);
		} catch (IOException e) {
			e.printStackTrace();
			MessageDialog.openError(this, Lang._txt()._Dialog_LanguageChanged_Title(),
					Lang._txt()._Dialog_LanguageChanged_Message());
		}
		
		// Update menu
		for (MenuItem languageItem : this.menuLanguages) {
			Object metaData = languageItem.getData("language");
			if (!(metaData instanceof Class)) {
				continue;
			}
			
			@SuppressWarnings("unchecked") // It IS CHECKED! See above...
			Class<? extends LanguagePackContainer> lang = (Class<? extends LanguagePackContainer>) metaData;
			
			if (lang != null && lang.equals(languageContainerClass)) {
				languageItem.setEnabled(false);
			}
			else {
				languageItem.setEnabled(true);
			}
		}

		// Display a message to the user
		LanguagePackContainer newLang = LanguagePack.getInstanceOfLanguage(languageContainerClass);
		MessageDialog.openInformation(this, newLang._Dialog_LanguageChanged_Title(),
				newLang._Dialog_LanguageChanged_Message());
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	protected void onClose(ShellEvent e) {
		super.onClose(e);

		// Stop AciSoft Simple
		aciSoftSimple.stop();

		// On some systems, the Java Virtual Machine appears to keep running, so
		// we schedule a forced exit to ensure the JVM also exits.
		Timer t = new Timer("Exit Timer", true);
		t.schedule(new TimerTask() {
			@Override
			public void run() {
                forceExit();
			}
		}, 5000);
	}

	private void forceExit() {
        System.out.println("WARNING: Process still running, will do System.exit(0).");
        System.exit(0);
    }

	public void updateScrolledComposite() {
		this.scrolledComposite
				.setMinSize(mediaFileSelectionGroup.computeSize(this.scrolledComposite.getBounds().x, SWT.DEFAULT));
	}

	@Override
	public void onUpdate() {
		// When this window gets disposed, remove it from the scheduler and
		// return to prevent SWTException dumps
		if (this.isDisposed()) {
			Scheduler.unregister(this);
			return;
		}

		this.getDisplay().syncExec(() -> {
            try {
                onUpdateInternally();
            } catch (SWTException e) {
                if (e.getMessage() != null && e.getMessage().contains("is disposed")) {
                    LOGGER.info("Ignored a 'is disposes' exception");
                } else {
                    LOGGER.error("Error updating window", e);
                }
            }
        });
	}

	private void onUpdateInternally() {
        // Update status in controller window
        boolean multipleMonitors = monitorManage.updateStatus();
        boolean controlsEnabled = multipleMonitors && aciPlayer.isWindowVisible();

        // Apply result of the check
        if (mediaFileSelectionGroup.getEnabled() != controlsEnabled) {
            mediaFileSelectionGroup.setEnabled(controlsEnabled);
        }
        if (playerControls.getEnabled() != controlsEnabled) {
            playerControls.setEnabled(controlsEnabled);
        }
        if (timeControl.getEnabled() != controlsEnabled) {
            timeControl.setEnabled(controlsEnabled);
        }

        updateScrolledComposite();
        playerControls.onUpdate();
        timeControl.onUpdate();
    }
}

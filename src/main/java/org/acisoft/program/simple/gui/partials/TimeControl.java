package org.acisoft.program.simple.gui.partials;

import org.acisoft.core.AciPlayer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.acisoft.core.PlayState;
import org.acisoft.core.Scheduler;
import org.acisoft.core.Time;
import org.acisoft.core.Updateable;
import org.acisoft.core.media.MediaEntry;
import org.acisoft.exception.IllegalStateException;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class TimeControl extends Group implements Updateable {
	private Label lblTimeCurrent;
	private Label lblTimeRemaining;
	private Label lblTimeTotal;
	private Scale sclTimeProgress;
	private Button btnDecr5s;
	private Button btnIncr5s;
	private Button btnDecr30s;
	private Button btnIncr30s;
	private Button btnDecr1m;
	private Button btnIncr1m;
	private TimeSetter timeSetter;

	private boolean enabled;
	private boolean scaleBeingDragged;
	private PlayState originalPlayState;

	private AciPlayer aciPlayer;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public TimeControl(Composite parent, int style) {
		this(parent, style, null);
	}

	/**
	 * Create the composite.
	 *  @param parent
	 * @param style
	 * @param aciPlayer
	 */
	public TimeControl(Composite parent, int style, AciPlayer aciPlayer) {
		super(parent, style);

		this.aciPlayer = aciPlayer;

		setLayout(new GridLayout(8, false));

		Label lblTimeInfo = new Label(this, SWT.NONE);
		lblTimeInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 8, 1));
		lblTimeInfo.setText(Lang._txt()._TimeControl_Title() + ":");

		Label lblLabelTimeCurrent = new Label(this, SWT.NONE);
		lblLabelTimeCurrent.setAlignment(SWT.CENTER);
		lblLabelTimeCurrent.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblLabelTimeCurrent.setText(Lang._txt()._TimeControl_CurrentTime());

		Label label = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 2);
		gd_label.heightHint = 50;
		label.setLayoutData(gd_label);

		Label lblLabelTimeRemaining = new Label(this, SWT.NONE);
		lblLabelTimeRemaining.setAlignment(SWT.CENTER);
		GridData gd_lblLabelTimeRemaining = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_lblLabelTimeRemaining.verticalIndent = 5;
		lblLabelTimeRemaining.setLayoutData(gd_lblLabelTimeRemaining);
		lblLabelTimeRemaining.setText(Lang._txt()._TimeControl_RemainingTime());

		Label label_1 = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
		GridData gd_label_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 2);
		gd_label_1.heightHint = 50;
		label_1.setLayoutData(gd_label_1);

		Label lblLabelTimeTotal = new Label(this, SWT.NONE);
		lblLabelTimeTotal.setAlignment(SWT.CENTER);
		lblLabelTimeTotal.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblLabelTimeTotal.setText(Lang._txt()._TimeControl_TotalTime());

		lblTimeCurrent = new Label(this, SWT.NONE);
		lblTimeCurrent.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		lblTimeCurrent.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblTimeCurrent.setAlignment(SWT.CENTER);
		lblTimeCurrent.setText("00:00:00,000");

		lblTimeRemaining = new Label(this, SWT.NONE);
		lblTimeRemaining.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		lblTimeRemaining.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblTimeRemaining.setAlignment(SWT.CENTER);
		lblTimeRemaining.setText("00:00:00,000");

		lblTimeTotal = new Label(this, SWT.NONE);
		lblTimeTotal.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		lblTimeTotal.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblTimeTotal.setAlignment(SWT.CENTER);
		lblTimeTotal.setText("00:00:00,000");

		sclTimeProgress = new Scale(this, SWT.NONE);
		sclTimeProgress.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				scaleTimeDragStart(e);
			}

			@Override
			public void mouseUp(MouseEvent e) {
				scaleTimeDragEnd(e);
			}
		});
		sclTimeProgress.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				scaleTimeDragging(e);
			}
		});
		sclTimeProgress.setPageIncrement(25);
		sclTimeProgress.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 8, 1));
		sclTimeProgress.setEnabled(false);

		btnDecr5s = new Button(this, SWT.NONE);
		btnDecr5s.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				substractTimeClick(e, new Time(0, 5, 0));
			}
		});
		btnDecr5s.setEnabled(false);
		GridData gd_btnDecr5s = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_btnDecr5s.widthHint = 50;
		btnDecr5s.setLayoutData(gd_btnDecr5s);
		btnDecr5s.setText("-5s");

		btnIncr5s = new Button(this, SWT.NONE);
		btnIncr5s.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				addTimeClick(e, new Time(0, 5, 0));
			}
		});
		btnIncr5s.setEnabled(false);
		GridData gd_btnIncr5s = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_btnIncr5s.widthHint = 50;
		btnIncr5s.setLayoutData(gd_btnIncr5s);
		btnIncr5s.setText("+5s");
		new Label(this, SWT.NONE);

		btnDecr30s = new Button(this, SWT.NONE);
		btnDecr30s.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				substractTimeClick(e, new Time(0, 30, 0));
			}
		});
		btnDecr30s.setEnabled(false);
		GridData gd_btnDecr30s = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_btnDecr30s.widthHint = 50;
		btnDecr30s.setLayoutData(gd_btnDecr30s);
		btnDecr30s.setText("-30s");

		btnIncr30s = new Button(this, SWT.NONE);
		btnIncr30s.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				addTimeClick(e, new Time(0, 30, 0));
			}
		});
		btnIncr30s.setEnabled(false);
		GridData gd_btnIncr30s = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_btnIncr30s.widthHint = 50;
		btnIncr30s.setLayoutData(gd_btnIncr30s);
		btnIncr30s.setText("+30s");
		new Label(this, SWT.NONE);

		btnDecr1m = new Button(this, SWT.NONE);
		btnDecr1m.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				substractTimeClick(e, new Time(1, 0, 0));
			}
		});
		btnDecr1m.setEnabled(false);
		GridData gd_btnDecr1m = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_btnDecr1m.widthHint = 50;
		btnDecr1m.setLayoutData(gd_btnDecr1m);
		btnDecr1m.setText("-1m");

		btnIncr1m = new Button(this, SWT.NONE);
		btnIncr1m.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				addTimeClick(e, new Time(1, 0, 0));
			}
		});
		btnIncr1m.setEnabled(false);
		GridData gd_btnIncr1m = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_btnIncr1m.widthHint = 50;
		btnIncr1m.setLayoutData(gd_btnIncr1m);
		btnIncr1m.setText("+1m");

		Label lblSetcurrentTime = new Label(this, SWT.NONE);
		lblSetcurrentTime.setAlignment(SWT.RIGHT);
		GridData gd_lblSetcurrentTime = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_lblSetcurrentTime.verticalIndent = 10;
		lblSetcurrentTime.setLayoutData(gd_lblSetcurrentTime);
		lblSetcurrentTime.setText(Lang._txt()._TimeControl_SetTime() + ":");
		new Label(this, SWT.NONE);

		timeSetter = new TimeSetter(this, SWT.NONE, aciPlayer);
		GridData gd_timeSetter = new GridData(SWT.LEFT, SWT.CENTER, false, false, 5, 1);
		gd_timeSetter.verticalIndent = 10;
		timeSetter.setLayoutData(gd_timeSetter);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Sets the "current {@link Time}" value.
	 * 
	 * @param t
	 *            The "current {@link Time}" value to set
	 */
	public void setCurrentTimeValue(Time t) {
		this.lblTimeCurrent.setText(t.toString());
	}

	/**
	 * Sets the "remaining {@link Time}" value.
	 * 
	 * @param t
	 *            The "remaining {@link Time}" value to set
	 */
	public void setRemainingTimeValue(Time t) {
		this.lblTimeRemaining.setText(t.toString());
	}

	/**
	 * Sets the "total {@link Time}" value.
	 * 
	 * @param t
	 *            The "total {@link Time}" value to set
	 */
	public void setTotalTimeValue(Time t) {
		this.lblTimeTotal.setText(t.toString());
	}

	/**
	 * Sets the current value of the {@link Time} scale.
	 * 
	 * @param value
	 *            The value to set for the {@link Time} scale
	 */
	public void setTimeScaleValue(int value) {
		this.sclTimeProgress.setSelection(value);
	}

	/**
	 * Sets the maximum value of the {@link Time} scale.
	 * 
	 * @param max
	 *            The maximum value for the {@link Time} scale
	 */
	public void setTimeScaleMax(int max) {
		this.sclTimeProgress.setMaximum(max);
	}

	public void scaleTimeChanged(MouseEvent e) {
		this.applyScaleValueToMediaPlayer();
	}

	public void scaleTimeDragStart(MouseEvent e) {
		PlayState state = PlayState.UNKNOWN;
		try {
			state = this.aciPlayer.getPlayState();
		} catch (IllegalStateException err) {
			err.printStackTrace();
			return;
		}
		this.originalPlayState = state;

		this.scaleBeingDragged = true;
	}

	public void scaleTimeDragEnd(MouseEvent e) {
		this.scaleTimeChanged(e);
		if (this.originalPlayState == PlayState.PLAYING) {
			this.aciPlayer.play();
		}

		this.scaleBeingDragged = false;
	}

	public void scaleTimeDragging(SelectionEvent e) {
		if (this.scaleBeingDragged) {
			this.aciPlayer.pause();
			this.applyScaleValueToMediaPlayer();
		}
	}

	public void applyScaleValueToMediaPlayer() {
		MediaEntry loadedMedia = this.aciPlayer.getLoadedMedia();
		if (loadedMedia == null) return;

		Time mediaLengthObj = loadedMedia.getLength();
		int mediaLength = mediaLengthObj.toMilliseconds();
		if (mediaLength > 0) {
			int selection = this.sclTimeProgress.getSelection();
			if (selection > mediaLength) selection = mediaLength;

			Time newTime = Time.fromMilliseconds(selection);
			this.aciPlayer.setTime(newTime);
		}
	}

	public void addTimeClick(MouseEvent e, Time t) {
		// Check preconditions
		// Get the PlayState if possible
		PlayState currentState = PlayState.UNKNOWN;
		try {
			currentState = this.aciPlayer.getPlayState();
		} catch (IllegalStateException err) {
			err.printStackTrace();
			return;
		}
		// The PlayState should be PLAYING or PAUSED, otherwise return. This
		// check also verifies media is loaded.
		if (currentState != PlayState.PLAYING && currentState != PlayState.PAUSED) return;

		// Get the length of the current media
		Time mediaLengthObj = this.aciPlayer.getLoadedMedia().getLength();
		int mediaLength = mediaLengthObj.toMilliseconds();

		// Get the current time
		Time currentTime = this.aciPlayer.getTime();

		// Add time
		int newTime = currentTime.toMilliseconds() + t.toMilliseconds();
		// Should be length at most
		if (newTime > mediaLength) newTime = mediaLength;

		// Create Time object
		Time newTimeObj = Time.fromMilliseconds(newTime);
		// Set time
		this.aciPlayer.setTime(newTimeObj);
	}

	public void substractTimeClick(MouseEvent e, Time t) {
		// Check preconditions
		// Get the PlayState if possible
		PlayState currentState = PlayState.UNKNOWN;
		try {
			currentState = this.aciPlayer.getPlayState();
		} catch (IllegalStateException err) {
			err.printStackTrace();
			return;
		}
		// The PlayState should be PLAYING or PAUSED, otherwise return. This
		// check also verifies media is loaded.
		if (currentState != PlayState.PLAYING && currentState != PlayState.PAUSED) return;

		// Get the length of the current media
		// Time mediaLengthObj =
		// this.acisoft.getPlayer().getLoadedMedia().getLength();
		// int mediaLength = mediaLengthObj.toMilliseconds();

		// Get the current time
		Time currentTime = this.aciPlayer.getTime();

		// Subtract time
		int newTime = currentTime.toMilliseconds() - t.toMilliseconds();
		// Should be 0 at least
		if (newTime < 0) newTime = 0;

		// Create Time object
		Time newTimeObj = Time.fromMilliseconds(newTime);
		// Set time
		this.aciPlayer.setTime(newTimeObj);
	}

	@Override
	public boolean isEnabled() {
		return this.getEnabled();
	}

	@Override
	public boolean getEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.sclTimeProgress.setEnabled(enabled);
		this.btnDecr5s.setEnabled(enabled);
		this.btnIncr5s.setEnabled(enabled);
		this.btnDecr30s.setEnabled(enabled);
		this.btnIncr30s.setEnabled(enabled);
		this.btnDecr1m.setEnabled(enabled);
		this.btnIncr1m.setEnabled(enabled);
		this.timeSetter.setEnabled(enabled);
		this.enabled = enabled;
	}

	@Override
	public void onUpdate() {
		if (this.aciPlayer == null) return;

		this.getDisplay().asyncExec(() -> {
            // Update current time
            Time currentTime = aciPlayer.getTime();
            int cTime = currentTime.toMilliseconds();
            if (cTime < 0) {
                currentTime = new Time();
                cTime = currentTime.toMilliseconds();
            }
            setCurrentTimeValue(currentTime);

            // Update max time / media length
            Time maxTime = new Time();
            int mTime = maxTime.toMilliseconds();
            MediaEntry entry = aciPlayer.getLoadedMedia();
            if (entry != null && entry.getLength().toMilliseconds() > 0) {
                maxTime = entry.getLength();
                mTime = maxTime.toMilliseconds();
            }
            setTotalTimeValue(maxTime);

            // Update remaining time
            if (mTime > cTime) {
                setRemainingTimeValue(Time.fromMilliseconds(mTime - cTime));
            } else {
                setRemainingTimeValue(new Time());
            }

            // Update scale
            if (sclTimeProgress.getMaximum() != mTime) sclTimeProgress.setMaximum(mTime);
            if (!scaleBeingDragged && sclTimeProgress.getSelection() != cTime) sclTimeProgress.setSelection(cTime);
            int pageIncr = mTime / 4;
            if (sclTimeProgress.getPageIncrement() != pageIncr) sclTimeProgress.setPageIncrement(pageIncr);
        });
	}
}

package org.acisoft.program.simple.gui.partials;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;

/**
 * Class used for resolution/Monitor testing.
 * 
 * TODO Currently unused!
 * 
 * @author regapictures
 *
 */
public class ResolutionManager extends Group {

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ResolutionManager(Composite parent, int style) {
		super(parent, style);
		setText("Show/test resolution (Presentation monitor)");
		setLayout(new GridLayout(2, false));

		Button btnDisplayResolutionBorder = new Button(this, SWT.CHECK);
		btnDisplayResolutionBorder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		btnDisplayResolutionBorder.setText("Display resolution border");

		Label lblBorderSize = new Label(this, SWT.NONE);
		lblBorderSize.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblBorderSize.setText("Border size:");

		Combo comboBorderSize = new Combo(this, SWT.NONE);
		comboBorderSize.setItems(new String[] { "1 pixel", "2 pixels", "5 pixels" });
		comboBorderSize.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		comboBorderSize.select(0);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}

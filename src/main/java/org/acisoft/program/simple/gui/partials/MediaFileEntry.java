package org.acisoft.program.simple.gui.partials;

import org.acisoft.core.AciPlayer;
import org.acisoft.core.MediaEntryFactory;
import org.acisoft.core.PlayState;
import org.acisoft.core.Scheduler;
import org.acisoft.core.Time;
import org.acisoft.core.Updateable;
import org.acisoft.core.media.FileMediaEntry;
import org.acisoft.core.media.MediaEntry;
import org.acisoft.exception.UnsupportedFileException;
import org.acisoft.program.simple.gui.SimpleControllerWindow;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import java.io.File;
import java.io.FileNotFoundException;

public class MediaFileEntry extends Composite implements Updateable {
	private int index;

	private Label lblIndex;
	private Text textFile;
	private Text txtMediaLength;
	private Button btnBrowse;
	private Button btnStart;

	private File selectedFile;
	private FileMediaEntry mediaEntry;
	private Thread loader;

	private MediaFileCollection parent;
	private SimpleControllerWindow controller;
	private AciPlayer aciPlayer;
	private MediaEntryFactory mediaEntryFactory;

	private boolean enabled;
	private Button btnLoad;
	private Button btnDelete;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MediaFileEntry(Composite parent, int style) {
		this(parent, style, null, null, null);
	}

	/**
	 * Create the composite.
     * @param parent
     * @param style
     * @param aciPlayer
     * @param mediaEntryFactory
     */
	public MediaFileEntry(Composite parent, int style, SimpleControllerWindow controller, AciPlayer aciPlayer,
                          MediaEntryFactory mediaEntryFactory) {
		super(parent, style);

		this.parent = (MediaFileCollection) parent;
		this.controller = controller;
		this.aciPlayer = aciPlayer;
        this.mediaEntryFactory = mediaEntryFactory;

        GridLayout gridLayout = new GridLayout(7, false);
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);

		lblIndex = new Label(this, SWT.NONE);
		lblIndex.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblIndex.setText("1.");
		this.index = 1;

		textFile = new Text(this, SWT.BORDER);
		textFile.setEditable(false);
		GridData gd_textFile = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_textFile.horizontalIndent = 5;
		textFile.setLayoutData(gd_textFile);

		txtMediaLength = new Text(this, SWT.BORDER | SWT.CENTER);
		txtMediaLength.setEditable(false);
		txtMediaLength.setText("00:00:00,000");
		GridData gd_txtMediaLength = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtMediaLength.widthHint = 65;
		txtMediaLength.setLayoutData(gd_txtMediaLength);

		btnBrowse = new Button(this, SWT.NONE);
		btnBrowse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				browseClick(e);
			}
		});
		btnBrowse.setText(Lang._txt()._FileBrowse() + "...");

		btnLoad = new Button(this, SWT.NONE);
		btnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				loadClick(e);
			}
		});
		btnLoad.setText(Lang._txt()._Media_Load());

		btnStart = new Button(this, SWT.NONE);
		btnStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				startClick(e);
			}
		});
		btnStart.setText(Lang._txt()._Media_Play());

		btnDelete = new Button(this, SWT.NONE);
		btnDelete.setToolTipText(Lang._txt()._MediaFileEntry_DeleteEntry_ToolTip());
		btnDelete.setEnabled(false);
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				deleteClick(e);
			}
		});
		btnDelete.setText("\u2715");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	public boolean getEnabled() {
		return this.isEnabled();
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		// super.setEnabled(enabled);
		// this.textFile.setEnabled(enabled); // Allow file selection, but
		// prevent playing
		// this.buttonBrowse.setEnabled(enabled);
		// this.btnLoad.setEnabled(enabled);

		this.btnStart.setEnabled(enabled);
		this.enabled = enabled;
	}

	private void browseClick(MouseEvent e) {
		if (this.controller == null) return;

		FileDialog dialog = new FileDialog(this.controller.getShell());
		dialog.setText(Lang._txt()._FileBrowser_SelectMedia_Title());
		String fstr = dialog.open();
		if (fstr == null) return;

		if (this.selectedFile == null) {
			this.getDisplay().asyncExec(parent::addEntry);
		}

		this.setFile(fstr);
	}

	private void loadClick(MouseEvent e) {
		if (this.selectedFile == null || this.mediaEntry == null) return;
		else if (!this.selectedFile.exists()) {
			this.showFileNotFoundMessage();
			return;
		}

		// Get current MediaEntry
		MediaEntry cme = aciPlayer.getLoadedMedia();

		// Get current status
		PlayState state;
		try {
			state = aciPlayer.getPlayState();
		} catch (org.acisoft.exception.IllegalStateException err) {
			err.printStackTrace();
			state = PlayState.UNKNOWN;
		}

		if (state == PlayState.PLAYING) {
			// Do not continue, otherwise awkward things (for the person
			// controlling this program) could happen...
			return;
		} else if ((cme instanceof FileMediaEntry)) {
			FileMediaEntry cfme = (FileMediaEntry) cme;

			// Is the selected media not the loaded one?
			if (cfme == null || !cfme.equals(this.mediaEntry)) {
				// No. Load the selected media.
				aciPlayer.play();
				aciPlayer.loadMedia(this.mediaEntry);
			}
		} else {
			// Overwrite current playing item.
			aciPlayer.loadMedia(this.mediaEntry);
		}
	}

	private void startClick(MouseEvent e) {
		if (this.selectedFile == null || this.mediaEntry == null) return;
		else if (!this.selectedFile.exists()) {
			this.showFileNotFoundMessage();
			return;
		}

		// Get current MediaEntry
		MediaEntry cme = aciPlayer.getLoadedMedia();

		if ((cme instanceof FileMediaEntry)) {
			FileMediaEntry cfme = (FileMediaEntry) cme;

			// Is the selected media already loaded?
			if (cfme != null && cfme.equals(this.mediaEntry)) {
				// Yes. Continue playing current media.
				aciPlayer.play();
			} else {
				// No. Overwrite current playing item.
				aciPlayer.playMedia(this.mediaEntry);
			}
		} else {
			// Overwrite current playing item.
			aciPlayer.playMedia(this.mediaEntry);
		}
	}

	private void deleteClick(MouseEvent e) {
		if (this.parent.getEntryCount() > 1) {
			this.parent.removeEntry(this);
		}
	}

	/**
	 * Sets the selected {@link File}.
	 * 
	 * @param file
	 *            The {@link File} to set
	 */
	public void setFile(String file) {
		if (file == null) {
			this.selectedFile = null;
		} else {
			File f = new File(file);

			if (!f.exists()) {
				this.showFileNotFoundMessage();
				return;
			}

			this.selectedFile = f;
			this.loadFileAsync();
		}

		// Update the GUI
		this.getDisplay().asyncExec(() -> {
            textFile.setText(selectedFile == null ? "" : selectedFile.getName());
            parent.updateEntries();
        });
	}

	/**
	 * Gets the currently selected {@link File}.
	 * 
	 * @return The currently selected {@link File}
	 */
	public File getFile() {
		return this.selectedFile == null ? null : new File(this.selectedFile.getAbsolutePath());
	}

	/**
	 * Return if this {@link MediaFileEntry} is empty (no file selected).
	 * 
	 * @return If this {@link MediaFileEntry} is empty (no file selected)
	 */
	public boolean isEmpty() {
		return this.getFile() == null;
	}

	/**
	 * Tries to cancel current asynchronous loading.
	 */
	private void cancelAsyncLoad() {
		if (this.loader == null) {
			return;
		} else {
			if (this.loader.isAlive()) {
				try {
					this.loader.interrupt();
					this.getDisplay().asyncExec(() -> txtMediaLength.setText("00:00:00,000"));
				} catch (SecurityException e) {
					System.err.println("ERROR: Cannot stop loader thread!");
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Starts the asynchronous loading of the currently selected media.
	 */
	private void loadFileAsync() {
		this.mediaEntry = null;

		if (this.loader == null) {
			this.prepareMediaLoaderThread();
		} else {
			this.cancelAsyncLoad();

			synchronized (this.loader) {
				this.prepareMediaLoaderThread();
			}
		}

		this.txtMediaLength.setText(Lang._txt()._Media_Loading() + "...");
		this.loader.setDaemon(true);
		this.loader.start();
	}

	/**
	 * Prepares the asynchronous loader thread so media can be loaded
	 * asynchronous.
	 */
	private synchronized void prepareMediaLoaderThread() {
		this.loader = new Thread(() -> {
            if (selectedFile != null) {
                // Try to read the selected media file
                try {
                    if (mediaEntry == null) {
                        mediaEntry = mediaEntryFactory.read(selectedFile);
                    } else {
                        synchronized (mediaEntry) {
                            mediaEntry = mediaEntryFactory.read(selectedFile);
                        }
                    }
                } catch (UnsupportedFileException e) {
                    e.printStackTrace();

                    // Cancel loading
                    cancelAsyncLoad();

                    // Unselect the file
                    setFile(null);

                    // Show not supported message to the user
                    showFileNotSupportedMessage();
                }

                // Cleanup entries, especially when we ended up in one of
                // the catch blocks above.
                parent.cleanupEntries();

                // Update the GUI
                if (mediaEntry != null) {
                    getDisplay().asyncExec(() -> {
                        Time t = mediaEntry.getLength();
                        if (t.toMilliseconds() > 0) txtMediaLength.setText(t.toString());
                        else {
                            // Length currently unknown

                            // Show unknown time
                            txtMediaLength.setText("??:??:??,???");

                            // Schedule watchdog
                            Scheduler.register(this);
                        }
                    });
                }
            }
        }, "MediaFile loader Thread #" + this.index);
	}

	public void setDisplayIndex(int index) {
		this.index = index;
		this.getDisplay().asyncExec(() -> lblIndex.setText(index + "."));
	}

	private void showFileNotFoundMessage() {
		this.getDisplay().syncExec(() -> MessageDialog.openError(getShell(), Lang._txt()._Dialog_Error_FileNotFound_Title(),
                Lang._txt()._Dialog_Error_FileNotFound_Message()));
	}

	private void showFileNotSupportedMessage() {
		this.getDisplay().syncExec(() ->
                MessageDialog.openError(getShell(), Lang._txt()._Dialog_Error_FileNotSupported_Title(),
                        Lang._txt()._Dialog_Error_FileNotSupported_Message()));
	}

	/**
	 * Sets whether this {@link MediaFileEntry} can be deleted by the user.
	 * 
	 * @param deleteAllowed
	 *            <code>true</code> to allow deletion, <code>false</code>
	 *            otherwise
	 */
	public void setAllowDelete(boolean deleteAllowed) {
		this.getDisplay().syncExec(() -> btnDelete.setEnabled(deleteAllowed));
	}

	@Override
	public void onUpdate() {
		MediaEntry entry = this.aciPlayer.getLoadedMedia();
		if (entry == null) return;
		else if (!(entry instanceof FileMediaEntry)) {
			// Another media type is already playing, stop watchdog
			Scheduler.unregister(this);
			return;
		}

		FileMediaEntry fEntry = (FileMediaEntry) entry;
		if (fEntry != null && fEntry.equals(this.mediaEntry)) {
			Time length = this.mediaEntry.getLength();

			if (length.toMilliseconds() > 0) {
				this.getDisplay().asyncExec(() -> txtMediaLength.setText(length.toString()));

				Scheduler.unregister(this);
			}
		}
	}
}

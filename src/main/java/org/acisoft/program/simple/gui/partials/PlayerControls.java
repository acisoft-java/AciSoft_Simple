package org.acisoft.program.simple.gui.partials;

import org.acisoft.core.AciPlayer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Text;
import org.acisoft.core.PlayState;
import org.acisoft.core.Scheduler;
import org.acisoft.core.Updateable;
import org.acisoft.core.media.MediaEntry;
import org.acisoft.exception.IllegalStateException;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class PlayerControls extends Group implements Updateable {
	private Text txtCurrentMedia;
	private Label lblPlayState;
	private Button btnStart;
	private Button btnPause;
	private Button btnStop;

	private boolean enabled;

	private PlayState currentPlayState;

	private AciPlayer aciPlayer;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PlayerControls(Composite parent, int style) {
		this(parent, style, null);
	}

	/**
	 * Create the composite.
	 *  @param parent
	 * @param style
	 * @param aciPlayer
	 */
	public PlayerControls(Composite parent, int style, AciPlayer aciPlayer) {
		super(parent, style);

		this.aciPlayer = aciPlayer;

		GridLayout gridLayout = new GridLayout(5, false);
		setLayout(gridLayout);

		txtCurrentMedia = new Text(this, SWT.BORDER);
		txtCurrentMedia.setEditable(false);
		this.setCurrentMediaName(null);
		txtCurrentMedia.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		lblPlayState = new Label(this, SWT.NONE);
		lblPlayState.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblPlayState.setSize(100, 15);
		lblPlayState.setAlignment(SWT.CENTER);
		GridData gd_lblPlayState = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
		gd_lblPlayState.minimumWidth = 70;
		gd_lblPlayState.widthHint = 70;
		lblPlayState.setLayoutData(gd_lblPlayState);
		this.setPlayState(PlayState.IDLE);

		btnStart = new Button(this, SWT.NONE);
		btnStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				playClick(e);
			}
		});
		GridData gd_btnStart = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_btnStart.horizontalIndent = 5;
		gd_btnStart.minimumWidth = 50;
		gd_btnStart.widthHint = 50;
		btnStart.setLayoutData(gd_btnStart);
		btnStart.setText(Lang._txt()._Media_Play());

		btnPause = new Button(this, SWT.NONE);
		btnPause.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				pauseClick(e);
			}
		});
		GridData gd_btnPause = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_btnPause.minimumWidth = 50;
		gd_btnPause.widthHint = 50;
		btnPause.setLayoutData(gd_btnPause);
		btnPause.setText(Lang._txt()._Media_Pause());

		btnStop = new Button(this, SWT.NONE);
		btnStop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				stopClick(e);
			}
		});
		GridData gd_btnStop = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_btnStop.minimumWidth = 50;
		gd_btnStop.widthHint = 50;
		btnStop.setLayoutData(gd_btnStop);
		btnStop.setText(Lang._txt()._Media_Stop());

		this.setEnabled(false);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Sets the displayed media name.
	 * 
	 * @param fileName
	 *            The media name to display
	 */
	public void setCurrentMediaName(String fileName) {
		if (fileName == null || fileName.equals("")) {
			this.txtCurrentMedia.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
			this.txtCurrentMedia.setEnabled(false);
			this.txtCurrentMedia.setText(Lang._txt()._PlayerControls_NoMediaLoaded());
		} else {
			this.txtCurrentMedia.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
			this.txtCurrentMedia.setEnabled(true);
			this.txtCurrentMedia.setText(fileName);
		}
	}

	/**
	 * Sets the displayed {@link PlayState}.
	 * 
	 * @param state
	 *            The new {@link PlayState} to display
	 */
	public void setPlayState(PlayState state) {
		this.lblPlayState.setText(this.getPlayStateString(state));
		this.currentPlayState = state;
	}

	@Override
	public boolean isEnabled() {
		return this.getEnabled();
	}

	@Override
	public boolean getEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.btnStart.setEnabled(enabled && this.aciPlayer.getLoadedMedia() != null);
		this.btnPause.setEnabled(enabled);
		this.btnStop.setEnabled(enabled);
		this.enabled = enabled;
	}

	private void playClick(MouseEvent e) {
		if (this.aciPlayer.getLoadedMedia() != null) {
			this.aciPlayer.play();
		}
	}

	private void pauseClick(MouseEvent e) {
		this.aciPlayer.pause();
	}

	private void stopClick(MouseEvent e) {
		this.aciPlayer.stop();
	}

	public String getPlayStateString(PlayState state) {
		String txtState = "";
		switch (state) {
			case IDLE:
				txtState = Lang._txt()._PlayState_Idle();
				break;
			case PLAYING:
				txtState = Lang._txt()._PlayState_Playing();
				break;
			case PAUSED:
				txtState = Lang._txt()._PlayState_Paused();
				break;
			case STOPPED:
				txtState = Lang._txt()._PlayState_Stopped();
				break;
			default:
				txtState = Lang._txt()._PlayState_Unknown();
		}

		return txtState;
	}

	@Override
	public void onUpdate() {
		if (this.aciPlayer == null) return;

		this.getDisplay().asyncExec(() -> {
            MediaEntry currentMedia = aciPlayer.getLoadedMedia();
            btnStart.setEnabled(enabled && currentMedia != null);

            String updatedMediaStr = (currentMedia == null ? null : currentMedia.toFriendlyString());
            if (updatedMediaStr != null && !updatedMediaStr.equals(txtCurrentMedia.getText()))
                setCurrentMediaName(updatedMediaStr);

            PlayState state;
            try {
                state = aciPlayer.getPlayState();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                state = PlayState.UNKNOWN;
            }

            if (currentPlayState != state) setPlayState(state);
        });
	}
}

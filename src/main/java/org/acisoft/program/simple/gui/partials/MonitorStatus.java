package org.acisoft.program.simple.gui.partials;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.acisoft.program.simple.gui.SimpleControllerWindow;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;

public class MonitorStatus extends Group {

	private Label lblConnected;
	private Label lblResolution;

	@SuppressWarnings("unused")
	private SimpleControllerWindow controller;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 * @param controller
	 */
	public MonitorStatus(Composite parent, int style, SimpleControllerWindow controller) {
		super(parent, style);

		this.controller = controller;

		setText("Scherm");
		setLayout(new GridLayout(1, false));

		lblConnected = new Label(this, SWT.NONE);
		lblConnected.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		this.setConnected(false);

		lblResolution = new Label(this, SWT.NONE);
		lblResolution.setText("0 x 0");

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setConnected(boolean connected) {
		if (connected) {
			this.lblConnected.setText("\u2713 " + Lang._txt()._MonitorStatus_MonitorConnected());
		} else {
			this.lblConnected.setText("\u2715 " + Lang._txt()._MonitorStatus_MonitorDisconnected());
		}
	}

	public void setResolution(Rectangle r) {
		this.setResolution(r.width, r.height);
	}

	public void setResolution(int width, int height) {
		this.lblResolution.setText(width + " x " + height);
	}
}

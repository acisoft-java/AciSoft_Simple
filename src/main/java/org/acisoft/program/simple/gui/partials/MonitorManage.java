package org.acisoft.program.simple.gui.partials;

import org.acisoft.core.AciPlayer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.acisoft.program.simple.gui.SimpleControllerWindow;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class MonitorManage extends Group {

	private MonitorStatus monitorStatus_1;
	private MonitorStatus monitorStatus_2;
	private Label lblInfo;
	private Button btnSchermToeeigenen;
	private boolean monitorOwned;

	private AciPlayer aciPlayer;
	private SimpleControllerWindow controller;

	private int previousMonitorAmount;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MonitorManage(Composite parent, int style) {
		this(parent, style, null);
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MonitorManage(Composite parent, int style, AciPlayer aciPlayer) {
		super(parent, style);

		if (parent instanceof SimpleControllerWindow) this.controller = (SimpleControllerWindow) parent;
		this.aciPlayer = aciPlayer;

		setText(Lang._txt()._MonitorManage_Title());
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);

		monitorStatus_1 = new MonitorStatus(this, SWT.NONE, this.controller);
		monitorStatus_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		monitorStatus_1.setText(Lang._txt()._MonitorManage_Monitor1_Title());

		monitorStatus_2 = new MonitorStatus(this, SWT.NONE, this.controller);
		monitorStatus_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		monitorStatus_2.setText(Lang._txt()._MonitorManage_Monitor2_Title());

		lblInfo = new Label(this, SWT.NONE);
		lblInfo.setText(Lang._txt()._MonitorManage_ExtendMonitorInfo());

		btnSchermToeeigenen = new Button(this, SWT.NONE);
		btnSchermToeeigenen.setEnabled(false);
		btnSchermToeeigenen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				toggleMonitorClick(e);
			}
		});
		this.setMonitorOwnedStatus(false);
		this.previousMonitorAmount = 0;

		this.updateStatus();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Updates the status of the monitors and takes the required actions.
	 * 
	 * @return If there is more than one monitor available
	 */
	public boolean updateStatus() {
		if (isDisposed()) {
		    return false;
        }

		Monitor[] monitors = this.getDisplay().getMonitors();
		int amount = monitors.length;

		if (amount > 0) {
			this.monitorStatus_1.setConnected(true);
			this.monitorStatus_1.setResolution(monitors[0].getBounds());
		} else {
			this.monitorStatus_1.setConnected(false);
			this.monitorStatus_1.setResolution(0, 0);
		}

		if (amount > 1) {
			this.btnSchermToeeigenen.setEnabled(true);
			this.monitorStatus_2.setConnected(true);
			this.monitorStatus_2.setResolution(monitors[1].getBounds());
		} else {
			this.btnSchermToeeigenen.setEnabled(false);
			this.monitorStatus_2.setConnected(false);
			this.monitorStatus_2.setResolution(0, 0);
		}

		if (this.previousMonitorAmount != amount) {
			this.layout(true, true);
			this.previousMonitorAmount = amount;
		}

		return (amount > 1);
	}

	public void setMonitorOwnedStatus(boolean owned) {
		if (owned) {
			this.btnSchermToeeigenen.setText(Lang._txt()._MonitorManage_FreeMonitor());
		} else {
			this.btnSchermToeeigenen.setText(Lang._txt()._MonitorManage_OccupyMonitor());
		}
		this.monitorOwned = owned;
	}

	private void toggleMonitorClick(MouseEvent e) {
		boolean newState = !this.monitorOwned;

		if (!newState) this.aciPlayer.stop();
		if (newState) {
		    aciPlayer.showWindow();
        } else {
		    aciPlayer.stop();
		    aciPlayer.hideWindow();
        }
		this.setMonitorOwnedStatus(newState);
	}
}

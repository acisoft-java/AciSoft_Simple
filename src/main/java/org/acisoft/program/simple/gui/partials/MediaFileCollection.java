package org.acisoft.program.simple.gui.partials;

import org.acisoft.core.AciPlayer;
import org.acisoft.core.MediaEntryFactory;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;

import java.util.ArrayList;
import java.util.List;

import org.acisoft.program.simple.gui.SimpleControllerWindow;
import org.acisoft.program.simple.lang.Lang;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;

public class MediaFileCollection extends Group {

	private List<MediaFileEntry> entries;

	private SimpleControllerWindow controller;
	private AciPlayer aciPlayer;
	private MediaEntryFactory mediaEntryFactory;

	private boolean enabled;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MediaFileCollection(Composite parent, int style) {
		this(parent, style, null, null, null);
	}

	/**
	 * Create the composite.
     * @param parent
     * @param style
     * @param aciPlayer
     * @param mediaEntryFactory
     */
	public MediaFileCollection(Composite parent, int style, SimpleControllerWindow controller, AciPlayer aciPlayer,
                               MediaEntryFactory mediaEntryFactory) {
		super(parent, style);

		this.controller = controller;
		this.aciPlayer = aciPlayer;
        this.mediaEntryFactory = mediaEntryFactory;

        this.entries = new ArrayList<>();
		this.enabled = true;

		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		this.setText(Lang._txt()._MediaFileCollection_Title());
		this.forceFocus();

		setLayout(new GridLayout(1, false));

		// Add one MediaFileEntry
		this.addEntry();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	public boolean getEnabled() {
		return this.isEnabled();
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		// super.setEnabled(enabled); // See MediaFileEntry.setEnabled for not
		// doing this
		for (MediaFileEntry entry : this.entries) {
			entry.setEnabled(enabled);
		}
		this.enabled = enabled;
	}

	/**
	 * Adds a new {@link MediaFileEntry} to this {@link MediaFileCollection}.
	 */
	public void addEntry() {
		MediaFileEntry mediaFileEntry = new MediaFileEntry(this, SWT.NONE, this.controller, this.aciPlayer, mediaEntryFactory);
		mediaFileEntry.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		mediaFileEntry.setDisplayIndex(this.entries.size() + 1);
		mediaFileEntry.setEnabled(this.isEnabled());

		this.entries.add(mediaFileEntry);
		this.layout(true, true);
	}

	/**
	 * <p>
	 * Updates all entries.
	 * </p>
	 * 
	 * <p>
	 * Takes care of:
	 * </p>
	 * <ul>
	 * <li>Updating the display index of each entry</li>
	 * <li>(Dis)Allowing deletion of each entry</li>
	 * </ul>
	 */
	public void updateEntries() {
		int size = this.entries.size();

		MediaFileEntry lastEmptyEntry = null;
		for (int i = 0; i < size; i++) {
			// Get entry
			MediaFileEntry entry = this.entries.get(i);
			// Set display index
			entry.setDisplayIndex(i + 1);
			// Allow deletion. This may be undone later.
			entry.setAllowDelete(true);
			// Store if empty
			if (entry.isEmpty()) {
				lastEmptyEntry = entry;
			}
		}

		if (lastEmptyEntry == null) {
			// No empty entry found, disallow deletion of the last entry
			this.entries.get(size - 1).setAllowDelete(false);
		} else {
			// Disallow deletion of the last EMPTY entry
			lastEmptyEntry.setAllowDelete(false);
		}
	}

	/**
	 * Removes the given {@link MediaFileEntry} from this
	 * {@link MediaFileCollection}
	 * 
	 * @param entry
	 *            The {@link MediaFileEntry} to remove
	 */
	public void removeEntry(MediaFileEntry entry) {
		if (entry == null) {
			return;
		}

		// Dispose the entry
		entry.getDisplay().syncExec(() -> entry.dispose());

		// Update the layout in this collection
		this.getDisplay().asyncExec(() -> layout(true));

		// Remove the entry from the list
		this.entries.remove(entry);

		// Update display indexes
		this.updateEntries();
	}

	/**
	 * Cleans up {@link MediaFileEntry}'s in this {@link MediaFileCollection}.
	 */
	public void cleanupEntries() {
		for (int i = this.entries.size() - 2; i >= 0; i--) {
			// Get the current entry count
			int currentSize = this.entries.size();
			if (currentSize < 2) {
				break;
			}

			// Get the current entry with 'i'
			MediaFileEntry entry = this.entries.get(i);
			// Get the next entry
			MediaFileEntry nextEntry = this.entries.get(i + 1);

			if (entry.isEmpty()) {
				// Delete the "next" entry if it is empty AND the current one is
				// empty as well
				if (nextEntry.isEmpty()) {
					this.removeEntry(nextEntry);
				}
				// Delete the current entry if it is empty AND if the next one
				// is not empty
				else {
					this.removeEntry(entry);
				}
			}
		}
	}

	/**
	 * Returns the amount of {@link MediaFileEntry}'s in this
	 * {@link MediaFileCollection}.
	 * 
	 * @return The amount of {@link MediaFileEntry}'s in this
	 *         {@link MediaFileCollection}
	 */
	public int getEntryCount() {
		return this.entries.size();
	}
}

package org.acisoft.program.simple.lang;

/**
 * <p>
 * Shorter alternative for {@link LanguagePack}.
 * </p>
 * 
 * <p>
 * Methods in this static class call the equivalent methods in
 * {@link LanguagePack}.
 * </p>
 * 
 * @author regapictures
 *
 */
public class Lang {

	private Lang() {
		// Static class
	}

	/**
	 * Calls {@link LanguagePack#_txt()} and returns the set
	 * {@link LanguagePack}.
	 * 
	 * @return The set {@link LanguagePack}
	 */
	public static LanguagePackContainer _txt() {
		return (LanguagePackContainer) LanguagePack._txt();
	}
}

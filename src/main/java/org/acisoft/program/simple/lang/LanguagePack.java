package org.acisoft.program.simple.lang;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.acisoft.program.simple.AciSoftSimple;

public class LanguagePack {

	protected static LanguagePackContainer instance;

	private static List<Class<? extends LanguagePackContainer>> availableLangs;

	private static final String LANGUAGE_FILE_NAME = "lang.dat";

	static {
		// Add available languages to the list
		LanguagePack.availableLangs = new ArrayList<>();
		LanguagePack.availableLangs.add(EnglishLanguagePack.class);
		LanguagePack.availableLangs.add(DutchLanguagePack.class);
	}

	private LanguagePack() {
		// Static class
	}

	/**
	 * Returns a list of the available languages.
	 * 
	 * @return A list of the available languages
	 */
	public static List<Class<? extends LanguagePackContainer>> getAvailableLanguages() {
		return new ArrayList<>(LanguagePack.availableLangs);
	}

	/**
	 * <p>
	 * Gets the current instance of the {@link LanguagePackContainer}.
	 * </p>
	 * 
	 * <p>
	 * Use this to access all Strings of the set language.
	 * </p>
	 * 
	 * @return
	 */
	public static LanguagePackContainer _txt() {
		if (LanguagePack.instance == null) {
			LanguagePack.setDefaultLanguage();
		}

		return LanguagePack.instance;
	}

	/**
	 * <p>
	 * Gets an instance of the given {@link LanguagePackContainer} class.
	 * </p>
	 * 
	 * <p>
	 * <b>NOTE:</b> this does not set the language, see {@link #set(Class)} or
	 * other language setting methods.
	 * </p>
	 * 
	 * @param languageContainerClass
	 *            The {@link LanguagePackContainer} class to get an instance of
	 * @return An instance of the given {@link LanguagePackContainer} class
	 */
	public static LanguagePackContainer getInstanceOfLanguage(
			Class<? extends LanguagePackContainer> languageContainerClass) {
		Constructor<? extends LanguagePackContainer> cons = null;
		try {
			cons = languageContainerClass.getConstructor();
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("Could not get constructor to set the given language!", e);
		}

		LanguagePackContainer lang = null;
		try {
			lang = cons.newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("Could not create new instance to set the given language!", e);
		}

		return lang;
	}

	/**
	 * Sets the given {@link LanguagePackContainer} class as the current
	 * language.
	 * 
	 * @param languageContainerClass
	 *            The {@link LanguagePackContainer} class of the language to set
	 * @return The created {@link LanguagePackContainer}
	 */
	public static LanguagePackContainer set(Class<? extends LanguagePackContainer> languageContainerClass) {
		LanguagePackContainer lang = LanguagePack.getInstanceOfLanguage(languageContainerClass);

		LanguagePack.instance = lang;
		return lang;
	}

	/**
	 * Gets the currently set language pack.
	 * 
	 * @return The currently set language pack
	 */
	public static Class<? extends LanguagePackContainer> getCurrentLanguage() {
		return LanguagePack.instance.getClass();
	}

	/**
	 * <p>
	 * Sets the language to the 'default' one.
	 * </p>
	 * 
	 * <p>
	 * Currently this is English.
	 * </p>
	 * 
	 * @return The created {@link LanguagePackContainer}
	 */
	public static LanguagePackContainer setDefaultLanguage() {
		return LanguagePack.setEnglish();
	}

	/**
	 * Sets the language to English and returns the created
	 * {@link LanguagePackContainer} .
	 * 
	 * @return The created {@link LanguagePackContainer}
	 */
	public static LanguagePackContainer setEnglish() {
		return LanguagePack.set(EnglishLanguagePack.class);
	}

	/**
	 * Sets the language to Dutch ("Nederlands") and returns the created
	 * {@link LanguagePackContainer} .
	 * 
	 * @return The created {@link LanguagePackContainer}
	 */
	public static LanguagePackContainer setDutch() {
		return LanguagePack.set(DutchLanguagePack.class);
	}

	/**
	 * <p>
	 * Gets the folder where the language setting is stored.
	 * </p>
	 * 
	 * <p>
	 * Requires access to {@link AciSoftSimple}'s instance and uses
	 * {@link AciSoftSimple#getJarFile()}.
	 * </p>
	 * 
	 * @return The folder where the language setting is stored
	 */
	public static File getLanguageSaveFolder() {
		AciSoftSimple aci = AciSoftSimple.getInstance();
		if (aci == null) {
			throw new NullPointerException("Reference to AciSoftSimple required for saving language to disk!");
		}

		File jar = aci.getJarFile();
		if (jar == null) {
			throw new NullPointerException("Location of the JAR-file required for saving language to disk!");
		}

		// Get the parent folder of the JAR-file
		File dir;
		if (jar.isFile()) {
			dir = new File(jar.getParent());
		} else {
			dir = jar;
		}

		return dir;
	}

	/**
	 * Saves the current set language selection to disk.
	 * @throws IOException When saving to disk fails
	 */
	public static void saveToDisk() throws IOException {
		if (LanguagePack.instance == null) {
			throw new NullPointerException("Cannot save language: No language selected yet.");
		}
		
		saveToDisk(LanguagePack.instance);
	}
	
	/**
	 * Saves the given language selection to disk.
	 * @param languageContainerClass The language to save to disk
	 * @throws IOException When saving to disk fails
	 */
	public static void saveToDisk(Class<? extends LanguagePackContainer> languageContainerClass) throws IOException {
		saveToDisk(LanguagePack.getInstanceOfLanguage(languageContainerClass));
	}
	
	/**
	 * Saves the given language selection to disk.
	 * @param lang The language to save to disk
	 * @throws IOException When saving to disk fails
	 */
	public static void saveToDisk(LanguagePackContainer lang) throws IOException {
		File loc = LanguagePack.getLanguageSaveFolder();
		if (loc == null) {
			throw new NullPointerException("Cannot use 'null' as language save location.");
		}

		String languageFile = loc.getAbsolutePath() + File.separator + LanguagePack.LANGUAGE_FILE_NAME;

		try (FileWriter writer = new FileWriter(languageFile)) {
			writer.write(lang.getLangCode());
		}
	}
	
	/**
	 * Loads the language to set from disk. If no file is found, or an unknown/invalid language code is read, calls {@link #setDefaultLanguage()} and returns.
	 * @throws IOException When loading from disk fails
	 */
	public static void loadFromDisk() throws IOException {
		File loc = LanguagePack.getLanguageSaveFolder();
		if (loc == null) {
			throw new NullPointerException("Cannot use 'null' as language save location.");
		}

		// Try to find the language file
		String languageFilePath = loc.getAbsolutePath() + File.separator + LanguagePack.LANGUAGE_FILE_NAME;
		File languageFile = new File(languageFilePath);
		if (!languageFile.exists()) {
			LanguagePack.setDefaultLanguage();
			return;
		}
		
		// Get the file length
		long fileLength = languageFile.length();
		
		// Prepare a read buffer
		int bufferSize = fileLength > 10 ? 10 : (int) fileLength;
		char[] readBuffer = new char[bufferSize];
		
		// Read the file
		try (FileReader reader = new FileReader(languageFilePath)) {
			reader.read(readBuffer);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File not found unexpectedly! Was there a race condition?");
		}
		
		// Create string & trim string
		String langCode = String.copyValueOf(readBuffer).trim();
		
		// Try to find the read language
		for (Class<? extends LanguagePackContainer> langClass : LanguagePack.availableLangs) {
			LanguagePackContainer lang = LanguagePack.getInstanceOfLanguage(langClass);
			
			// Known language?
			if (lang.getLangCode().equalsIgnoreCase(langCode)) {
				System.out.println("Found configured language '" + lang.getLanguageNameInEnglish() + "', setting as language...");
				
				// Yes! Set the language.
				LanguagePack.set(langClass);
				return;
			}
		}
		
		// No known/valid language found, so set default language
		LanguagePack.setDefaultLanguage();
	}
}

package org.acisoft.program.simple.lang;

import org.acisoft.program.simple.AciSoftSimple;

public class EnglishLanguagePack extends LanguagePackContainer {

	@Override
	public String getLangCode() {
		return "en";
	}

	@Override
	public String getLanguageNameInEnglish() {
		return "English";
	}

	@Override
	public String getLanguageName() {
		return "English";
	}

	/*
	 * Text data
	 */
	
	@Override
	public String _Dialog_FailedToSetLanguage_Title() {
		return "Failed to change language";
	}
	
	@Override
	public String _Dialog_FailedToSetLanguage_Message() {
		return "Failed to change language:\nCould not save to disk. "
					+ "Do I have the proper write permissions? Try running " + AciSoftSimple.APP_NAME +" as an Administrator.";
	}
	
	@Override
	public String _Dialog_FailedToLoadLanguage_Title() {
		return "Failed to load language";
	}
	
	@Override
	public String _Dialog_FailedToLoadLanguage_Message() {
		return "Failed to load language unexpectly. \n\n" + AciSoftSimple.APP_NAME + " will exit. Please try again later.";
	}

	@Override
	public String _Dialog_LanguageChanged_Title() {
		return "Language changed";
	}

	@Override
	public String _Dialog_LanguageChanged_Message() {
		return "The language has been changed to English.\n\nTo apply the change, you will have to restart "
				+ AciSoftSimple.APP_NAME + ".";
	}

	@Override
	public String _Dialog_VLCnotFound_Title() {
		return "Cannot find libraries";
	}

	@Override
	public String _Dialog_VLCnotFound_Message() {
		return "The program cannot find the required VLC libraries.\n\n" + AciSoftSimple.APP_NAME + " will exit.";
	}

	@Override
	public String _Dialog_Not64Bit_Title() {
		return "64-bit required";
	}

	@Override
	public String _Dialog_Not64Bit_Message(int bitSize) {
		return "The program must be executed with 64-bit Java, but it was started with " + bitSize + "-bit Java.\n\n"
				+ AciSoftSimple.APP_NAME + " will exit.";
	}

	@Override
	public String _Menu_Language() {
		return "Language";
	}

	@Override
	public String _TimeControls_Title() {
		return "Time Control";
	}

	@Override
	public String _MediaFileCollection_Title() {
		return "File selection";
	}

	@Override
	public String _FileBrowse() {
		return "Browse";
	}

	@Override
	public String _Media_Load() {
		return "Load";
	}

	@Override
	public String _Media_Play() {
		return "Play";
	}

	@Override
	public String _Media_Pause() {
		return "Pause";
	}

	@Override
	public String _Media_Stop() {
		return "Stop";
	}

	@Override
	public String _Media_Loading() {
		return "Loading";
	}

	@Override
	public String _MediaFileEntry_DeleteEntry_ToolTip() {
		return "Remove selection (not the file)";
	}

	@Override
	public String _FileBrowser_SelectMedia_Title() {
		return "Select a media file";
	}

	@Override
	public String _Dialog_Error_FileNotFound_Title() {
		return "File not found";
	}

	@Override
	public String _Dialog_Error_FileNotFound_Message() {
		return "Cannot find the selected file. Was it moved/deleted meanwhile?";
	}

	@Override
	public String _Dialog_Error_FileNotSupported_Title() {
		return "File not supported";
	}

	@Override
	public String _Dialog_Error_FileNotSupported_Message() {
		return "The selected file is not supported.";
	}

	@Override
	public String _MonitorManage_Title() {
		return "Monitor management";
	}

	@Override
	public String _MonitorManage_Monitor1_Title() {
		return "Monitor 1 (Controller)";
	}

	@Override
	public String _MonitorManage_Monitor2_Title() {
		return "Monitor 2 (Display)";
	}

	@Override
	public String _MonitorManage_ExtendMonitorInfo() {
		return "Use mode \"Extend\".";
	}

	@Override
	public String _MonitorManage_OccupyMonitor() {
		return "Occupy monitor";
	}

	@Override
	public String _MonitorManage_FreeMonitor() {
		return "Free monitor";
	}

	@Override
	public String _MonitorStatus_Title() {
		return "Monitor";
	}

	@Override
	public String _MonitorStatus_MonitorConnected() {
		return "Connected";
	}

	@Override
	public String _MonitorStatus_MonitorDisconnected() {
		return "Disconnected";
	}

	@Override
	public String _PlayState_Idle() {
		return "Idle";
	}

	@Override
	public String _PlayState_Playing() {
		return "Playing";
	}

	@Override
	public String _PlayState_Paused() {
		return "Paused";
	}

	@Override
	public String _PlayState_Stopped() {
		return "Stopped";
	}

	@Override
	public String _PlayState_Unknown() {
		return "Unknown";
	}

	@Override
	public String _PlayerControls_Title() {
		return "General controls";
	}

	@Override
	public String _PlayerControls_NoMediaLoaded() {
		return "No media loaded";
	}

	@Override
	public String _TimeControl_Title() {
		return "Current media's time data";
	}

	@Override
	public String _TimeControl_CurrentTime() {
		return "Current time";
	}

	@Override
	public String _TimeControl_RemainingTime() {
		return "Remaining time";
	}

	@Override
	public String _TimeControl_TotalTime() {
		return "Total time";
	}

	@Override
	public String _TimeControl_SetTime() {
		return "Set time";
	}

	@Override
	public String _TimeSetter_SetButton() {
		return "Set";
	}

	/*
	 * public String _() { return ""; }
	 */
}

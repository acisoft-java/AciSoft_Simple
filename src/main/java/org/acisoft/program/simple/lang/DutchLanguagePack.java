package org.acisoft.program.simple.lang;

import org.acisoft.program.simple.AciSoftSimple;

public class DutchLanguagePack extends EnglishLanguagePack {

	@Override
	public final String getLangCode() {
		return "nl";
	}

	@Override
	public final String getLanguageNameInEnglish() {
		return "Dutch";
	}

	@Override
	public final String getLanguageName() {
		return "Nederlands";
	}

	/*
	 * Text data
	 */
	
	@Override
	public String _Dialog_FailedToSetLanguage_Title() {
		return "Taal wijzigen mislukt";
	}
	
	@Override
	public String _Dialog_FailedToSetLanguage_Message() {
		return "Taal wijzigen mislukt:\nKon taal niet opslaan. "
					+ "Heb ik de juiste schrijfrechten? Probeer " + AciSoftSimple.APP_NAME +" uit te voeren als Administrator.";
	}
	
	@Override
	public String _Dialog_FailedToLoadLanguage_Title() {
		return "Kon taal niet laden";
	}
	
	@Override
	public String _Dialog_FailedToLoadLanguage_Message() {
		return "Kon taal niet laden. \n\n" + AciSoftSimple.APP_NAME + " zal worden afgesloten. "
				+ "Probeer het later opnieuw alstublieft.";
	}

	@Override
	public final String _Dialog_LanguageChanged_Title() {
		return "Taal gewijzigd";
	}

	@Override
	public final String _Dialog_LanguageChanged_Message() {
		return "De taal is gewijzigd naar Nederlands.\n\nOm de verandering toe te passen, moet u "
				+ AciSoftSimple.APP_NAME + " herstarten.";
	}

	@Override
	public String _Dialog_VLCnotFound_Title() {
		return "Kan bibliotheken niet vinden";
	}

	@Override
	public String _Dialog_VLCnotFound_Message() {
		return "Het programma kan de vereiste VLC-bibliotheken niet vinden.\n\n" + AciSoftSimple.APP_NAME
				+ " zal worden afgesloten.";
	}

	@Override
	public String _Dialog_Not64Bit_Title() {
		return "64-bit vereist";
	}

	@Override
	public String _Dialog_Not64Bit_Message(int bitSize) {
		return "Het programma moet uitgevoerd worden met 64-bit Java, maar het werd gestart met " + bitSize
				+ "-bit.\n\n" + AciSoftSimple.APP_NAME + " zal worden afgesloten.";
	}

	@Override
	public String _Menu_Language() {
		return "Taal";
	}

	@Override
	public final String _TimeControls_Title() {
		return "Tijdcontrole";
	}

	@Override
	public final String _MediaFileCollection_Title() {
		return "Bestandenselectie";
	}

	@Override
	public final String _FileBrowse() {
		return "Bladeren";
	}

	@Override
	public final String _Media_Load() {
		return "Laad";
	}

	@Override
	public final String _Media_Play() {
		return "Start";
	}

	@Override
	public final String _Media_Pause() {
		return "Pauze";
	}

	@Override
	public final String _Media_Stop() {
		return "Stop";
	}

	@Override
	public final String _Media_Loading() {
		return "Laden";
	}

	@Override
	public final String _MediaFileEntry_DeleteEntry_ToolTip() {
		return "Verwijder selectie (niet het bestand)";
	}

	@Override
	public final String _FileBrowser_SelectMedia_Title() {
		return "Selecteer een mediabestand";
	}

	@Override
	public final String _Dialog_Error_FileNotFound_Title() {
		return "Kan bestand niet vinden";
	}

	@Override
	public final String _Dialog_Error_FileNotFound_Message() {
		return "Kan het geselecteerde bestand niet vinden. Is het ondertussen verplaatst/verwijderd?";
	}

	@Override
	public final String _Dialog_Error_FileNotSupported_Title() {
		return "Bestand niet ondersteund";
	}

	@Override
	public final String _Dialog_Error_FileNotSupported_Message() {
		return "Het geselecteerde bestand wordt niet ondersteund.";
	}

	@Override
	public final String _MonitorManage_Title() {
		return "Schermbeheer";
	}

	@Override
	public final String _MonitorManage_Monitor1_Title() {
		return "Scherm 1 (Bedieningsscherm)";
	}

	@Override
	public final String _MonitorManage_Monitor2_Title() {
		return "Scherm 2 (Presentatiescherm)";
	}

	@Override
	public final String _MonitorManage_ExtendMonitorInfo() {
		return "Gebruik modus \"Uitbreiden\".";
	}

	@Override
	public final String _MonitorManage_OccupyMonitor() {
		return "Scherm toe-eigenen";
	}

	@Override
	public final String _MonitorManage_FreeMonitor() {
		return "Scherm vrij maken";
	}

	@Override
	public final String _MonitorStatus_Title() {
		return "Scherm";
	}

	@Override
	public final String _MonitorStatus_MonitorConnected() {
		return "Aangesloten";
	}

	@Override
	public final String _MonitorStatus_MonitorDisconnected() {
		return "Niet aangesloten";
	}

	@Override
	public final String _PlayState_Idle() {
		return "Inactief";
	}

	@Override
	public final String _PlayState_Playing() {
		return "Afspelend";
	}

	@Override
	public final String _PlayState_Paused() {
		return "Gepauzeerd";
	}

	@Override
	public final String _PlayState_Stopped() {
		return "Gestopt";
	}

	@Override
	public final String _PlayState_Unknown() {
		return "Onbekend";
	}

	@Override
	public final String _PlayerControls_Title() {
		return "Algemene bediening";
	}

	@Override
	public final String _PlayerControls_NoMediaLoaded() {
		return "Geen media geladen";
	}

	@Override
	public final String _TimeControl_Title() {
		return "Tijdgegevens van de huidige media";
	}

	@Override
	public final String _TimeControl_CurrentTime() {
		return "Huidige tijd";
	}

	@Override
	public final String _TimeControl_RemainingTime() {
		return "Resterende tijd";
	}

	@Override
	public final String _TimeControl_TotalTime() {
		return "Totale tijd";
	}

	@Override
	public final String _TimeControl_SetTime() {
		return "Stel tijd in";
	}

	@Override
	public final String _TimeSetter_SetButton() {
		return "Stel in";
	}

	/*
	 * public final String _() { return ""; }
	 */
}

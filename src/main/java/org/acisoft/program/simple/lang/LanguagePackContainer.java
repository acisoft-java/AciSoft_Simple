package org.acisoft.program.simple.lang;

/**
 * <p>
 * Interface containing all methods that need to be implemented by a language
 * pack.
 * </p>
 * 
 * <p>
 * Language pack classes <b>should not directly implement this class</b>; they
 * should extend the {@link EnglishLanguagePack} and overwrite the methods
 * there.
 * </p>
 * 
 * @author regapictures
 *
 */
public abstract class LanguagePackContainer {

	/**
	 * Returns the default language pack.
	 * 
	 * @return The default language pack
	 */
	public static Class<? extends LanguagePackContainer> getDefaultLanguage() {
		return EnglishLanguagePack.class;
	}

	/*
	 * Abstract text functions
	 */

	public abstract String getLangCode();

	public abstract String getLanguageNameInEnglish();

	public abstract String getLanguageName();
	
	public abstract String _Dialog_FailedToSetLanguage_Title();
	
	public abstract String _Dialog_FailedToSetLanguage_Message();
	
	public abstract String _Dialog_FailedToLoadLanguage_Title();
	
	public abstract String _Dialog_FailedToLoadLanguage_Message();

	public abstract String _Dialog_LanguageChanged_Title();

	public abstract String _Dialog_LanguageChanged_Message();

	public abstract String _Dialog_VLCnotFound_Title();

	public abstract String _Dialog_VLCnotFound_Message();

	public abstract String _Dialog_Not64Bit_Title();

	public abstract String _Dialog_Not64Bit_Message(int bitSize);

	public abstract String _Menu_Language();

	public abstract String _TimeControls_Title();

	public abstract String _MediaFileCollection_Title();

	public abstract String _FileBrowse();

	public abstract String _Media_Load();

	public abstract String _Media_Play();

	public abstract String _Media_Pause();

	public abstract String _Media_Stop();

	public abstract String _Media_Loading();

	public abstract String _MediaFileEntry_DeleteEntry_ToolTip();

	public abstract String _FileBrowser_SelectMedia_Title();

	public abstract String _Dialog_Error_FileNotFound_Title();

	public abstract String _Dialog_Error_FileNotFound_Message();

	public abstract String _Dialog_Error_FileNotSupported_Title();

	public abstract String _Dialog_Error_FileNotSupported_Message();

	public abstract String _MonitorManage_Title();

	public abstract String _MonitorManage_Monitor1_Title();

	public abstract String _MonitorManage_Monitor2_Title();

	public abstract String _MonitorManage_ExtendMonitorInfo();

	public abstract String _MonitorManage_OccupyMonitor();

	public abstract String _MonitorManage_FreeMonitor();

	public abstract String _MonitorStatus_Title();

	public abstract String _MonitorStatus_MonitorConnected();

	public abstract String _MonitorStatus_MonitorDisconnected();

	public abstract String _PlayState_Idle();

	public abstract String _PlayState_Playing();

	public abstract String _PlayState_Paused();

	public abstract String _PlayState_Stopped();

	public abstract String _PlayState_Unknown();

	public abstract String _PlayerControls_Title();

	public abstract String _PlayerControls_NoMediaLoaded();

	public abstract String _TimeControl_Title();

	public abstract String _TimeControl_CurrentTime();

	public abstract String _TimeControl_RemainingTime();

	public abstract String _TimeControl_TotalTime();

	public abstract String _TimeControl_SetTime();

	public abstract String _TimeSetter_SetButton();

}
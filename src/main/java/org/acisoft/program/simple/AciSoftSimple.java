package org.acisoft.program.simple;

import org.acisoft.core.AciSoftCore;
import org.acisoft.core.VlcjAciSoftCore;
import org.acisoft.core.gui.WindowRunner;
import org.acisoft.exception.IncompatibleBitArchitectureException;
import org.acisoft.exception.VlcNotFoundException;
import org.acisoft.program.simple.gui.SimpleControllerWindow;
import org.acisoft.program.simple.lang.Lang;
import org.acisoft.program.simple.lang.LanguagePack;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class AciSoftSimple {
	private static final Logger LOGGER = LoggerFactory.getLogger(AciSoftSimple.class);

	public static final String APP_NAME = "Gemsoft - Audio and Video";
	public static final String VERSION = "0.1.0";

	private AciSoftCore core;

	private SimpleControllerWindow controllerWindow;
	private Display controllerDisplay;
	private Thread controllerThread;

	private static AciSoftSimple instance;

	public AciSoftSimple() {
		this.core = new VlcjAciSoftCore();

		AciSoftSimple.instance = this;
	}

	public void run() {
		loadLanguage();
		bootCore();
        this.core.boot();
		startController();
		waitForControllerReady();

		try {
			this.controllerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void waitForControllerReady() {
		boolean waitMsg = true;
		while (true) {
			if (this.controllerWindow != null) {
				LOGGER.debug("\r\t\t\t\rControllerWindow created.");
				break;
			} else if (waitMsg) {
				LOGGER.debug("\rWaiting for ControllerWindow creation...");
				waitMsg = false;
			}

			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				LOGGER.error("[WARNING] Wait loop for waiting on ControllerWindow creation has been terminated " +
                        "by a InterruptedException: " + e.getMessage());
				break;
			}
		}
	}

	private void startController() {
		this.controllerThread = new Thread(() -> {
            controllerDisplay = new Display();
            controllerWindow = new SimpleControllerWindow(controllerDisplay, this, core.getPlayer(),
                    core.getMediaEntryFactory());

            // Create and run runner
            WindowRunner runner = new WindowRunner(controllerWindow);
            runner.run();
        }, "ControllerWindow");
		this.controllerThread.start();
	}

	private void bootCore() {
		try {
			this.core.boot();
		} catch (VlcNotFoundException e) {
			e.printStackTrace();

			// Eclipse SWT GUI libraries not yet loaded: use Java Swing instead
			JOptionPane.showMessageDialog(null, Lang._txt()._Dialog_VLCnotFound_Message(),
					Lang._txt()._Dialog_VLCnotFound_Title(), JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} catch (IncompatibleBitArchitectureException e) {
			e.printStackTrace();

			/*
			 * Eclipse SWT GUI libraries not yet loaded + NOT SUPPORTED: Acisoft
			 * Simple doesn't ship with (all) required 32-bit SWT libraries -->
			 * use Java Swing instead
			 */
			JOptionPane.showMessageDialog(null, Lang._txt()._Dialog_Not64Bit_Message(e.getDetectedBitSize()),
					Lang._txt()._Dialog_Not64Bit_Title(), JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}

	private void loadLanguage() {
		try {
			LanguagePack.loadFromDisk();
		} catch (IOException e1) {
			e1.printStackTrace();
			// Eclipse SWT GUI libraries not yet loaded: use Java Swing instead
			JOptionPane.showMessageDialog(null, Lang._txt()._Dialog_FailedToLoadLanguage_Message(),
					Lang._txt()._Dialog_FailedToLoadLanguage_Title(), JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}

	/**
	 * Stops AciSoft Simple. Use only for exiting and disposing the application.
	 */
	public void stop() {
		this.core.stop();
	}

	/**
	 * Returns the last created instance of this class.
	 * 
	 * @return The last created instance of this class
	 */
	public static AciSoftSimple getInstance() {
		return AciSoftSimple.instance;
	}

	public File getJarFile() {
	    return core.getJarFile();
    }
}

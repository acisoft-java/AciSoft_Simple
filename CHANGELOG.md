# AciSoft Simple Changelog

## v0.1.0 (2016-09-23)
* Multilanguage support
* Added English language 

## v0.0.2 (2016-08-17)
* Added check for 64-bit Java Virtual Machine

## v0.0.1 (First version)
* Basic functionalities:
	* Toggle 2nd monitor playback window (is black when nothing is playing)
	* Monitor status display up to two monitors (when in expand mode)
	* Auto updating display status + player window responds accordingly ((dis)appear) when environment (e.g. monitors) changes
	* Multiple file selection (files not yet being checked if they are valid video/audio files!)
	* Length of selected files will be displayed. (If not, there's a small chance of you can't play it)
	* Play, pause, stop
	* Advanced time display & control **when playing/paused** (VLC doesn't support time control while stopped). Includes: 6 buttons for adding subtracting 5s, 30s or 1min; going to a specified time.
	* Supported language: Dutch
	* (Guess that's it for now)
* *JAR-file* available! See install.md for how to install AciSoft Simple on your computer.
To install CSoft Simple:

1. Install Java **64bit**
2. Create a new directory on a desired location.
3. Copy the JAR-file (found in the folder 'jar' in git) to that new directory.
4. Copy the folder 'vlc' (in AciSoft **Core**) to that new directory. (Do NOT extract the files in vlc in the new directory, but leave them in the folder 'vlc')

Done!
# AciSoft Simple


## Description
Program allowing you to play video's on a 2nd monitor (in "expand" mode) without dragging windows around, and with the ability to blacken the 2nd monitor when not playing anything.

Acisoft is the newer version of CSoft (the name "CSoft" is already taken, too bad...).

AciSoft_Simple depends on AciSoft_Core.

## Screenshot
![Controller window](assets/img/controller.png "Controller window")


## Versions

### Current: v0.1.0
* Basic functionalities:
	* Toggle 2nd monitor playback window (is black when nothing is playing)
	* Monitor status display up to two monitors (when in expand mode)
	* Auto updating display status + player window responds accordingly ((dis)appear) when environment (e.g. monitors) changes
	* Multiple file selection (files not yet being checked if they are valid video/audio files!)
	* Length of selected files will be displayed. (If not, there's a small chance of you can't play it)
	* Play, pause, stop
	* Advanced time display & control **when playing/paused** (VLC doesn't support time control while stopped). Includes: 6 buttons for adding or subtracting 5s, 30s or 1min; and fields for going to a specified time.
	* Supported languages: English, Dutch
	* (Guess that's it for now)
* *JAR-file* available! See install.md for how to install AciSoft Simple on your computer.

## Upcoming

### Short term
* ~~Resolution border display (for projectors having issues with varying video sizes, causing them to adapt to the video size instead of the monitor size)~~ **Dropped for now**

### Long term
* Self updating

Some day, there will be a version "AciSoft" that is more advanced.
For example, it will support:
* Auto-starting playlist 
	- Including: start item #n at time t, so calculate when item #1 should start to achieve this
* Custom media like countdowns (or clocks)
* Remote control possibilities (mobile)
* Etc.